package com.library.user;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	
    @Id @GeneratedValue
    Long id;
    private String name;
    private String surname;
    private String login;
    private String password;
 
    public User() {
    }
 
    public User(String name, String sur, String log, String pass) {
    this.name = name;
    this.surname = sur;
    this.login = log;
    this.password = pass;
    }
    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
    public String toString() {
        return name + " ";
    }
}
