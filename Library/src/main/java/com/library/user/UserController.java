package com.library.user;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

public class UserController {
	  @Autowired
	    private UserDao userDao;
	  
	  @RequestMapping(value="/register")
	    public ModelAndView registerUser(HttpServletRequest request) {
	        // Handle a new guest (if any):
	        String name = request.getParameter("name");
	        String sur = request.getParameter("surname");
	        String log = request.getParameter("login");
	        String pass = request.getParameter("password");
	        if (name != null)
	            userDao.persist(new User(name, sur, log, pass));
	 
	        // Prepare the result view (register.jsp):
	        return new ModelAndView("register.jsp", "userDao", userDao);
	    }
}
